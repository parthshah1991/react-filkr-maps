import React, { Component } from 'react';
import styles from './Map.scss';

export default class MapView extends Component {

	constructor() {
		super()
		
	}

	componentDidMount() {
		const myLatlng = {lat: 18.9456, lng: 72.7933};

		this.map = new window.google.maps.Map(this.refs.map, {
			zoom: 5,
			center: myLatlng
		});

		window.google.maps.event.addListener(this.map, 'click', (event) => {
			this.placeMarker(event.latLng);
		});
	}

	placeMarker(location) {
		if (this.marker) {
			this.marker.setMap(null);
		}
		this.marker = new window.google.maps.Marker({
			position: location, 
			map: this.map
		});
		this.props.onSelection(false, location.lat(), location.lng())
	}

	
	render() {
		return (
			<div className={styles.viewCntr}>
				<div className="map-container" style={mapStyle} ref="map"></div>
				<p>Click on the map to set marker and get results</p>
			</div>
		)
	}
}
const mapStyle = {
	width: '400px',
	height: '200px',
	display: 'inline-block'
}

