import React, { Component } from 'react';
import {SearchBox, PhotoList, MapView} from '../';
import styles from './App.scss';
import axios from 'axios'

export default class App extends Component {

	constructor() {
		super()
		this.state = {
			view: 'normal',
			searchResp: null,
			favourites: [],
			toSearch: true
		}
		this.searched = this.searched.bind(this);
		this.setNormalView = this.setNormalView.bind(this);
		this.setFavView = this.setFavView.bind(this);
		this.onFavClick = this.onFavClick.bind(this);
		this.toggleSearchView = this.toggleSearchView.bind(this);
	}

	searched(isSearch, arg1, arg2) {
		let qp = ''
		if (isSearch) {
			this.searchText = arg1
			qp = `text=${arg1}`
		} else {
			qp = `lat=${arg1}&lon=${arg2}`
			this.searchText = `lat=${arg1} and lon=${arg2}`
		}
		axios.get('https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=fd07e4d2c35b40a417766e6a98855319&'+ qp +'&per_page=15&format=json&nojsoncallback=1')
			.then((response) => {
				this.setState({
					searchResp: response.data
				})
			})
			.catch(() => {
				this.setState({
					searchResp: false
				})
			});
	}

	setFavView() {
		if(this.state.view !== 'fav') {
			this.setState({
				view: 'fav'
			})
		}
	}

	setNormalView() {
		if(this.state.view !== 'normal') {
			this.setState({
				view: 'normal'
			})
		}
	}

	onFavClick(pic, toAdd) {
		let favouritesRef = this.state.favourites.splice(0);
		if (toAdd) {
			favouritesRef.push(pic)
		} else {
			favouritesRef.splice(favouritesRef.indexOf(pic), 1)
		}

		this.setState({
			favourites: favouritesRef
		})
	}

	toggleSearchView() {
		this.setState({
			toSearch: !this.state.toSearch
		})
	}
	
	render() {
		const searchText = `Searched for ${this.searchText}`
		return (
			<div className={styles.app}>
				<h1> Test Project </h1>
				<h3>
					<span className={styles.tab} onClick={this.setNormalView}>Flickr Search</span>
					<span className={styles.tab} onClick={this.setFavView}>Favourites ({this.state.favourites.length})</span>
				</h3>
				{
					this.state.view == 'normal' &&
						<div>
							{
								this.state.toSearch &&
									<div>
										<SearchBox onSearchClick={this.searched} />
										<span className={styles.link} onClick={this.toggleSearchView}>Map View</span>
									</div>
							}
							{
								!this.state.toSearch &&
									<div>
										<MapView onSelection={this.searched} />
										<span className={styles.link} onClick={this.toggleSearchView}>Search View</span>
									</div>
							}
							{
								this.state.searchResp === false &&
									<div className="error">API failed</div>
							}

							{	
								this.state.searchResp &&

									<PhotoList
										photos = {this.state.searchResp.photos.photo}
										total = {this.state.searchResp.photos.total}
										onFavClick = {this.onFavClick}
										favList = {this.state.favourites}
										title = {searchText}
									/>
							}
						</div>
				}
				{
					this.state.view == 'fav' &&
						<PhotoList
							photos = {this.state.favourites}
							total = {this.state.favourites.length}
							onFavClick = {this.onFavClick}
							favList = {this.state.favourites}
							title = "Favourites"
						/>
				}
				
			</div>
		);
	}
}

