import React, { Component } from 'react';
import styles from './Search.scss';

export default class SearchBox extends Component {

	constructor() {
		super()
		this.searchClick = this.searchClick.bind(this);
		this.handleKeyPress = this.handleKeyPress.bind(this);
	}

	searchClick() {
		const value = this.refs.input.value.trim()
		if(value) {
			this.props.onSearchClick(true, value)
		}
	}

	handleKeyPress(e) {
		if(e.key == 'Enter') {
			this.searchClick()
		}
	}
	
	render() {
		return (
			<div className={styles.inline}>
				<input className={styles.searchInput} placeholder="Serch Image on flickr" ref="input" onKeyPress={this.handleKeyPress}/>
				<div className={styles.seachButton} onClick={this.searchClick}>Search</div>
			</div>
		);
	}
}

