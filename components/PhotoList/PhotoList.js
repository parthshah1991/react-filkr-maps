import React, { Component } from 'react';
import classNames from 'classnames'
import styles from './Photo.scss';

export default class PhotoList extends Component {

	render() {
		const {
			total,
			photos,
			onFavClick,
			favList,
			title
		} = this.props

		return (
			<div>
				<h1> {title} </h1>
				<h3>Total Images found: {total}, showing {photos.length}</h3>
				{
					photos.length !== 0 && 
						photos.map((pic) => {
							let isfav = false
							if (favList.indexOf(pic) !== -1) {
								isfav = true
							}
							return <SinglePicture picture={pic} onFav={onFavClick} isFav={isfav} key={pic.id} />
						})
				}
				{
					photos.length === 0 &&

						<div className="empty">No results</div>
				}
			</div>
		);
	}
}


class SinglePicture extends Component {

	constructor(props) {
		super()
		this.state = {
			liked: props.isFav
		}
		this.favourite = this.favourite.bind(this);
	}

	favourite() {
		this.props.onFav(this.props.picture, !this.state.liked)
		this.setState({
			liked: !this.state.liked
		})
	}

	render() {
		const {
			farm,
			server,
			id,
			secret,
			title
		} = this.props.picture

		const imgUrl = `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}_m.jpg`;
		const likeClass = classNames(styles.likeIcon, {
			[styles.liked]: this.state.liked
		})


		return (
			<div className={styles.picContainer}>
				<div className={styles.imgageContainer}>
					<img className={styles.pictureImage} src={imgUrl} alt="pic.title"/>
					<div className={likeClass} onClick={this.favourite}>Fav</div>
				</div>
				<span className={styles.rightSideContainer}>
					<span className="">{title}</span>
				</span>
			</div>
		)
	}
}